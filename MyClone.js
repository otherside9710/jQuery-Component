//Dev By Julio Peña

$.component(".MyClone", [], {
    PARENT: "parent",

    init: ($self) =>{
        $self.$parent = $($self.$.attr($self.PARENT));
        $self.clone();
    },

    clone: ($self) => {
        $self.$.html($self.$parent.get(0).outerHTML);
    }
});
